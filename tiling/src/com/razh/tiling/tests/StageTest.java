package com.razh.tiling.tests;

import com.razh.tiling.TilingMeshStage;

public abstract class StageTest {
	public abstract void load(TilingMeshStage stage);
}
